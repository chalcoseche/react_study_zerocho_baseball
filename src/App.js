import React, { Component } from 'react';
import Try from './Try';

// 숫자 4개를 랜덤하게 뽑는 함수
function getNumbers() {
  const candidate = [1, 2, 3, 4, 5, 6, 7, 8, 9]; // 9가지의 숫자중에서
  const array = [];
  for (let i = 0; i < 4; i += 1) {
    const chosen = candidate.splice(
      // 랜덤한 인덱스 숫자를 "하나" 뽑아 chosen에 넣고
      Math.floor(Math.random() * (9 - i)), // random 의 수를 -1 하는 이유 :
      // 배열의 8개인데 숫자 9나오면 undefined 들어 갈 수 있음.
      1,
    )[0];
    array.push(chosen); // array 에 push함. ( 총 4개의 배열을 가진 array 생성)
  }
  return array; // 함수돌리면 4개의 배열이 나옴
}

class App extends Component {
  // 컨스트럭터 안적어두됨.
  state = {
    result: '',
    value: '',
    answer: getNumbers(), // 숫자 4개 뽑는 함수 리턴값. // ex : [1,3,5,7]
    tries: [], // 시도횟수 이때 js환경처럼 push 쓰면 안됨.
    // const array =[];
    // array.push(1) : array [1]
    // 리액트가 무엇이 변했는지 감지를 못함 arr === arr  : true
    // const array2 =[...array,새로운걸 넣어줌]
    // array [1,새로운걸 넣어줌]
    // array === array2
    // 리액트의 렌더링 하는 기준 : 과거 state와 현재state가 다를 경우 리렌더링.
  };

  onSubmitForm = (e) => {
    // 버튼을 눌렀을 때
    e.preventDefault();
    // 값이 맞는지 비교
    if (this.state.value === this.state.answer.join('')) {
      // 맞춘경우 (홈런일경우)
      this.setState({
        result: '홈런!',
        // 리액트에서는 배열추가를 해도 안먹힌다 push 안댐
        tries: [
          ...this.state.tries, // 과거 state복사
          { try: this.state.value, result: '홈런!' }, // 현재 state 새로 추가
          // 를 복사하여 tries에 넣는다.
        ],
      });
      // 홈런했으니 게임 다시 시작
      alert('게임을 다시 시작합니다!');
      this.setState({
        value: '',
        answer: getNumbers(),
        tries: [],
      });
    } else {
      // 숫자가 틀렸을 경우
      const answerArray = this.state.value.split('').map((v) => parseInt(v));
      let strike = 0;
      let ball = 0;
      if (this.state.tries.length >= 9) {
        this.setState({
          result: `10번 실패 답은 ${this.state.answer.join(',')}였습니다.`,
        });
        alert('게임을 다시 시작합니다!');
        this.setState({
          value: '',
          answer: getNumbers(),
          tries: [],
        });
      } else {
        // 몇볼 몇 스트라이크 계산
        for (let i = 0; i < 4; i += 1) {
          if (answerArray[i] === this.state.asnwer[i]) {
            strike += 1;
          } else if (this.state.answer.includes(answerArray[i])) {
            ball += 1;
          }
        }
        this.setState({
          tries: [
            ...this.state.tries,
            {
              try: this.state.value,
              result: `${strike}스트라이크,${ball}볼입니다`,
            },
          ],
        });
      }
    }
  };

  onChangeInput = (e) => {
    console.log(this.state.answer);
    this.setState({
      value: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <h1>{this.state.result}</h1>
        <form onSubmit={this.onSubmitForm}>
          <input
            maxLength={4}
            value={this.state.value}
            onChange={this.onChangeInput}
          />
        </form>
        <div>
          시도:
          {this.state.tries.length}
        </div>
        {/* ul과 li 부분이 map부분을 사용할 부분 */}
        {/* map은 리액트상에서 반복문 */}
        <ul>
          {/* 배열을 하나 만든다  */}
          {/* 배열메서드 .map사용 */}
          {/* 빈 배열 안에 원하는 반복할것을 넣는다 */}
          {/* 배열안에 아이템은 콜백함수 매개변수로 호출한다. */}
          {/* {['사과', '바나나', '포도', '귤', '감', '배', '딸기'].map((v, i) => (
            <>
              <li>{v}</li>
              <li>
                <b>사과</b>- 맛있다
              </li> */}
          {/* 바뀌는 부분이 두개씩 있을경우 고칠수있는 여러가지 방법이있다 */}
          {/*  */}
          {/* 1. 2차원배열 사용 */}
          {/* 2차원 배열로 바꾸는방법 [['사과','맛있다'],['딸기','맛없다'],['바나나','떫다'],] */}
          {/*  */}
          {/* 이차원배열일경우 배열자체가 v 가 된다 */}
          {/* {v[0]},{v[1]} */}
          {/*  */}
          {/* 2.객체로 사용하는방법 */}
          {/* 보통은 이런방법을 많이 사용한다. [{fruit:'사과',taste:'맛있다'},{fruit:'바나나',taste:'떫다'},] */}
          {/*  */}
          {/* 이경우 v는 객체 자체다. */}
          {/* {v.fruit},{v.taste} */}
          {/*  */}
          {/* 이런식으로 반복문을 돌린다. (가독성이 좋아보이지도않음) */}
          {/* 가독성 좋게만드는방법 :props를 이용한다. */}
          {/* map을 돌릴때 항상 key prop이 있어야함. */}
          {/* each child in a list should have a unique key prop */}
          {/* key={v.fruit} 화면에 표시는 안되지만 성능 최적화할때 필요  */}
          {/* 리액트가 key를 보고 같은 컴포넌트인지 아닌지 판단함 */}
          {/* key값을 고유하게 해줘야함. ex) {v.fruit + v.taste} */}
          {/* key값이 같아도 에러 실무에서 매우 귀찮지만 어쩔 수 없다. */}
          {/* 리액트 단점: 반복문이 좀 어렵다  */}
          {/* callbackfn : (v,i) 를 통해  인덱스[i]를 사용 할 수 있다. */}
          {/* 귀찮아서 key에 i를 넣으면 key의 이유는 성능 최적화인데 성능의 문제가 생길 수 있음 */}
          {/* {key=i}는 비추천 ! */}
          {/* 단, 요소가 추가만 되는 배열인 경우 i를 써도 무방함(삭제X) */}
          {/* 컴포넌트분리 와 props  */}
          {/* 가독성있게 표현; 배열을 this.fruits 로 작성뒤 ,객체를 밖으로 빼준다. */}
          {/* 반복문의 return 부분도 엄청 길어질수있다 */}
          {/* 이부분을 다른 컴포넌트로 빼서 가독성을 좋게 만들 수있다 */}
          {/* Try.jsx */}

          {/* <Try value={v} index={i} key={v.fruit + v.taste} />
            </>
          ))}
          ; */}
          {this.state.tries.map(
            (
              v,
              i, // 이때 v는 tries에 새로 추가된 객체들
            ) => (
              <Try key={`${i + 1}차시도 :`} tryInfo={v} /> // key 에 인덱스 활용은 안좋은예
            ),
          )}
        </ul>
      </div>
    );
  }
}

export default App;
