import React, { Component } from 'react';

class Try extends Component {
  render(props) {
    return (
      // 길어지는 컨텐츠를 다른 컴포넌트로 빼준다.
      // 그리고 App.js 에 jsx 확장자 <Try/>를 작성한다.
      // <li>
      //   <b>{this.props.value.fruit}</b> -{this.props.index}
      //   <b>{this.props.value.fruit}</b> -{this.props.index}
      //   <b>{this.props.value.fruit}</b> -{this.props.index}
      //   <b>{this.props.value.fruit}</b> -{this.props.index}
      // </li>
      // 파일을 따로따로 쪼개서 한줄로 정리가 됨
      // 성능문제때문에도 쪼개는 이유가 있음
      // 이때 li 태그 안에 v와 i를 Try.jsx로 전달이 안됨,
      // 사용하려면 "props기능"을 사용해야함.
      // <Try value={v} index={i} /> by App.js
      // 기존데이터를 프롭스로 전달
      // 입문자들은 큰 컴포넌트에서 작은 컴포넌트로 쪼개는 연습을 한다 ( 탑 - 다운 )
      // 바텀-업 방식이 어려움.
      // 3-5주석과 메서드바인딩
      // key값이 없다고 또 오류가뜨니 try jsx key값을 입력해준다.
      // 리액트의 대부분의 문제는 props에서 일어난다
      // 자식 부모간이 아니라   조상 - 손자 컴포넌트사이에서 문제가 생길 수 있다.
      // 이때 사용되는게 Redux, Context 등이 있다.
      // 주석관련...음
      // 화살표함수를 안쓰고싶으면 ,class 에 constructor 를 작성할것.
      // extends가 있으니 super()할것, bind도 해야할것
      // this.onSubmitForm = this.onSubmitForm.bind(this);
      // this.onChangeInput = this.onChangeInput.bind(this);
      // ( 오래된 코드, 해괴함 8ㅅ8 )
      // );
      <li>
        <div>{this.props.tryInfo.try}</div>
        <div>{this.props.tryInfo.try}</div>
      </li>
    );
  }
}

export default Try;
