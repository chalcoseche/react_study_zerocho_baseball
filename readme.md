#### `section2`

# 숫자야구를 통해 _props_ 배워보기

##### - js로 배운 웹게임들 리액트로 변환 해보기.

###### - 2022년에는 Hot이 없어짐.

###### - `ReactDOM.createRoot(...).render(<jsx/></jsx>);` 사용 할 것

> ## **import와 require 차이**

**require**: Node.js의 모듈 시스템

`module.exports = Numberbaseball;`를 넣어주면, require를 통해 가져올 수 있다.

###### import와 require가 서로 **살짝** 호환이 되기는 함.

`import React,{Component} from 'react;`
`const React = require('react');`

**const { Component}= React;** : 구조분해 문법  
(exports되는 게 **객체**,**배열**이면 구조 분해 할 수 있다.)

변수나 값같은걸 export를 따로 할 수있다.  
(export const hello = 'hello')

**default 로 가져온 것들은 : import ;  
default가 아닌것 : import {};**

##### 엄밀히 export default 와 module.exports 다르지만 리액트에선 호환이 된다 정도로 알면 된다.

babel 이 import 를 사용 할 수 있게 만들어줌 .

esm 인 import 를 사용하자.

> ## 리액트 반복문(map)

###### - 리액트로 변환할 때, 바뀌는 부분을 파악 할 것 (state).

**Class 컴포넌트에선 state변화시, render()내부가 재 실행됨.**

- maxLength 로 작성하기 ( html 에선 소문자)

- value 와 onChange는 세트로 써주기(안쓸거면 defaultValue)

- 메서드 함수들은 화살표 함수로 하기.  
  (화살표 안쓰면 constructor를 작성 해야함)

**메서드 map은 리액트에서 반복문을 사용하는 방법.**

```
[1,2,3,4,5].map(()=>{
return (
<tag>내용</tag>
);
})
```

배열의 길이 만큼 리턴이 실행됨.(= 반복)

배열의 벨류를 태그안에 넣어 활용 할 수 있음.

```
['가','나','다','라','마'].map((v)=>{
return (
<tag>v</tag>
);
})
```

반복되는 태그들을 배열로 만들어줌.

활용예제 :)

```
<li>가</li>
<li>나</li>
<li>다</li>
<li>라</li>
<li>마</li>
<li>바</li>

// 반복되는 태그들을 배열로 묶어줌
-> ['가','나','다','라','마','바']

// 변수선언
--> const Ganadara = ['가','나','다','라','마','바']

// map활용
---> Ganadara.map((v)=>{
return (
<tag>v</tag>
);
})
```

> ## 리액트 반복문 2 (key)

```
<li><b>가</b>-아</li>
<li><b>나</b>-야</li>
<li><b>다</b>-어</li>
<li><b>라</b>-여</li>
<li><b>마</b>-오</li>
<li><b>바</b>-요</li>
```

일 경우, 바뀌는 부분을 배열로 만든다.

- 리액트가 반복문 문법이 깔끔하지 않다.

#### 바뀌는부분이 2개 이상일때.

**1. 2차원 배열로 만든다.**

> - 바뀌는 부분을 배열에 넣는다

> > ```
> > const test =
> > [
> >   ['가','아']
> >   ['나','야']
> >   ['다','어']
> >   ['라','여']
> >   ['마','오']
> >   ['바','요']
> >   ]
> > ```

> - map 메서드 작성시,파라미터 v는
> - 배열 자체가 v가 되므로 앞 문자열은[0],뒷문자열은[1]이됨.

> ###### - v는 rowIndex.

> > ```
> > test.map((v)=>{
> > return (
> > <tag><b>{v[0]}</b>-{v[1]}</tag>
> > );
> > })
> > ```

**2. 객체로 만든다.**

> > ```
> > const test2 =
> > [
> >   {first:'가',second:'아'},
> >   {first:'나',second:'야'},
> >   {first:'다',second:'어'},
> >   {first:'라',second:'여'},
> >   {first:'마',second:'오'},
> >   {first:'바',second:'요'},
> >   ]
> > ```

> - 이럴경우 파라미터v는
> - 객체 하나가 v가 된다.

> > ```
> > test2.map((v)=>{
> > return (
> > <tag><b>{v.first}</b>-{v.second}</tag>
> > );
> > })
> > ```

이렇게 사용 할 수 있지만, 가독성 측면에서 좋지않음  
props를 이용해 해결 할 수 있다. ( 가독성 UP, 성능UP)

#### 반복문 사용시 key 를 추가해야함. key를 작성할땐 고유값을 넣는다.

(리액트에서 key를 통해 , 같은 컴포넌트인지 아닌지 판단함 )

###### 요소가 추가만되는 배열 인 경우 i를 사용해두 됨.

> ## 컴포넌트 분리와 props

가독성을 위해 배열과 객체를 위로 빼준다.

반복된 태그를 컴포넌트로 뺼 수 있다.

컴포넌트를 따로 뺐을 경우 파라미터를 가져올 방법은 **props를 통해서 가져온다.**

**콜백 함수의 v와 i를 props로 보낸다**

```
this.fruits.map((v,i)=> {
<Try value={v} index={i}/>
})
```

**자식컴포넌트 Try**

```
return (
  <tag>
  <tag>{this.props.value.fruit} - {this.props.index}</tag>
  </tag>
)
```

value,index props를 this.props.value, this.props.index 를 통해 가져온 모습

#### 리액트 숙달되기 위해

###### 탑다운 방식;

1. 한 컴포넌트에 다 작성한다.
2. (보통) 반복문 단위로 분리를 하여, 자식컴포넌트로 빼준다.

> ## 컴포넌트 분리와 props

- 리액트의 대부분 문제는 props에서 나온다.
- 부모-자식 뿐만 아니라, 할아버지..손자...증조할아버지...😵
- 해결은 *Redux,Context,MobX*를 사용함.

###### 화살표함수를 사용하지 않고 메서드를 사용 하려면 ,

###### constructor,super(),bind(this) 를 사용해야함(옛날)

###### 옛날 코드 분석을 위해 알고 넘어갈 것.

> ## 숫자야구 완성하기

- 리액트에서는 배열에다가 값을 넣을때 **push 쓰면안됨.**  
  리액트 내부에서 배열의변화를 감지 못함

**배열의 불변성.**

`const array2 =[...array, 2]`를 통해 배열을 새로 만들어 추가하고  
`array === array2` 로 변화를 줘야함 (렌더를 위해)

- Class 컴포넌트에선 구조분해를 통해 this.state를 생략 할 수 있다.

````
const {value,tries } = this.state;
const {tryInfo} = this.props;```
````

- this를 사용하지 않은 함수들은 class 밖으로 빼는것이 보기 좋다.(재사용도 가능)

- maps는 자바스크립트의 꽃 : 1:1 대응 시켜 **변화** 시켜 줄수있음
  ###### ( 원시값,배열,객체...->JSX,원시값 )

> ## Class -> Hooks 전환하기 (useState lazy init)

- this.props 를 hooks에선 (매개변수) 안에 작성해준다

`const Try =(props) => {}...`

###### 구조분해 역시 가능하다 ({props})

props.tryInfo.try  
tryInfo.try

---

**옛날값으로 현재값을 사용 할때는 함수형 SetState를 사용 할 것.**  
❌

```
this.setState({

tries: [...this.state.tries,{try:value,result:''}]
})
```

✅

```
this.state((prevState)=>{
return {
  tries: [...this.state.tries,{try:value,result:''}]
}
})
```

공부할 땐 HOOKS,Class 변환하는 연습 할것 (리액트 익히기)

===

### 미래에서온 제로초

###### :온 이유

`const [answer,setAnswer] = useState(getNumber());`❌

함수 컴포넌트 특성상 매번 렌더링시, 전체부분이 실행됨.

그럼 getNumber함수가 매번 작동하여 새로운 숫자를 만들어냄

###### 다행히도, 매번 작동해도, 첫 한번의 숫자들만 생성하기때문에 작동엔 상관X

( useState가 getNumbers에 리턴값을 answer에 넣기 때문. 리렌더링시 함수가 실행되도  
할당된 answer값은 변하지 않기 때문 두번째 리턴값부터 무시 )

**하지만, 쓸데없이 매번 실행되는것 자체가 문제임**

해결법은 useState()자리에 함수호출이 아닌 함수명을 넣을 것

`const [answer,setAnswer] = useState(getNumber);`✅ //lazy init

===

> ## React Devtools

- 저번시간에 배운 props 활용에 문제가있다 : 렌더링이 자주일어나서 성능 저하가 있다.
- 설치후, 개발자도구에서 react 작동,태그들을 확인할 수 있다.

* 배포모드(소스코드 압축, 최적화) 변경법 webpack 에서 mode:'production'  
  process.env.NODE_ENV = 'production' 을 추가한다.

> ## shouldComponentUpdate

- devtool로 렌더링을 확인할 시, 원하지 않는곳이 렌더링되는걸 확인 할 수 있다.  
  (다른컴포넌트가 리렌더링되는 문제점을 찾을 수 있다.)

- 문제점 해결을위해 먼저 리액트 렌더링 원리를 알자

- setState와 props 가 변할때마다 렌더링이 되지만,  
  **setState만 호출되도 리렌더링을 시도한다.**

- 방지 하기위해 **shouldComponentUpdate(nextProps,nextState,nextContext)** 를 작성한다.  
  (리액트 지원메서드)
- 직접 어떤 경우에 렌더링을 해줘야할지 **조건**을 작성 해야함.

```
shouldComponentUpdate(nextProps,nextState,nextContext){
  if(this.state.counter !== nextState.counter){ // 현재state의값과 미래의 state값이 다르면
    return true; // 렌더링 실행
  }
  return false; // 그렇지 않을경우 렌더링 무시
}
```

> ## 억울한 자식 리렌더링 막기(PureComponent와 memo)

- 위에 방식이 복잡할 경우 조금더 편한 방법

`{Component} from 'react' => {PureComponent}from'react'` 로 변경 작성 한다.  
`class Test extends PureComponent`

- PureComponent란, shouldComponentUpdate를 알아서 구현한 방식.
  state가 여러개 있으면, 바뀌었는지 아닌지 판단한다.

- **단점** : state에 객체나 참조관계가있는 경우, 진짜 바뀌었는지 판단이 어려워 작동이 제대로  
  되지 않을 수 있다.  
  **(현재 array와 미래array를 같다고 착각함
  해결법: [...array,추가값]을 사용하여 해결)**

##### state {a:1}에서 setState역시 {a:1}로 할때, 새로 렌더링 하므로 state에 객체 구조를 안 쓰는게 좋음.

===

### 미래에서온 제로초

###### 온 이유;

원치 않는 부분의 리렌더링 막기 재설명

숫자야구의 로그가 리렌더링 되는 부분은 부모컴포넌트(NumberBaseball.jsx)

**부모컴포넌트가 리렌더링시 , 자식 컴포넌트(Try.jsx)도 무조건 리렌더링 되기 때문**

자식 컴포넌트 리렌더링은 **state변화,props변화** 그리고 **부모컴포넌트의 변화**시 한다.

PureComponent는 이러한 현상을 막아줄수있다 **state변화 ,props변화가 있을시에만 리렌더링 되게 해줌**

### 단, PureComponent는 class 컴포넌트에서만 사용 가능

(Try.jsx에서는 사용 불가능)

함수 컴포넌트에서도 PureComponent와 기능을 사용하는 리액트 컴포넌트가 존재

### memo.

함수 컴포넌트를 memo로 감싸주면 된다  
❌

```
import React from 'react';
const Try () => {
  return();
};
```

✅

```
import React, {memo} from 'react';
const Try memo(() => {
  return();
});
```

**memo는 부모로부터 물려받은 리렌더링을 막아준다 !!**  
memo사용시 컴포넌트의 이름이 이상하게 출력된다.  
자식 컴포넌트 하단에 `Try.displayName ='Try';`로 컴포넌트명을 재설정 해준다.

i.e ) 부모컴포넌트의 리렌더링을 막으려면  
**함수 컴포넌트 = memo  
클래스 컴포넌트 = PureComponent 사용**

===

PureComponent 와 shouldComponentUpdate의 차이점  
shouldComponentUpdate : 요구사항의 **커스터마이징**이 가능함.

#### 성능최적화시에 PureComponent가 좋기 때문에, 리액트 공부시 활용 해 볼것.

( 성능 문제 생길 때 )

> ## React.createRef

- Class 컴포넌트의 ref활용

```
//input의 focus를 원함
1. <input ref={this.inputRef}>
2. input;
3. inputRef = (c) =>this.input =c;
4. 원하는곳에 this.inputRef.focus();
```

- 함수 컴포넌트의 ref활용 (current 추가작성)

```
1. const inputEl = useRef(null); //선언
2. inputEl.current.focus();

```

헷갈릴 수있으니, class에서 current를 사용 할 수 있는 방법이 있다.  
(hooks와 class의 통일성)

```
3. inputRef = (c) =>this.input =c;
=> New. inputRef = createRef();
=> New. 4. 원하는곳에 this.inputRef.current.focus();
```

- setState를 함수로 작성하면 예전state값말고도 더 미세한 작업(다른 동작)을 할 수 있다.

> ## props와 state 연결하기

- this.setState는 render() 밖에서 사용하기 ( 무한 반복 )

* **자식컴포넌트가 props를 변경하면 안된다.** 부모가 바꿔야함.( 원칙 )

- 바꿔야 할 경우, 부모에서 받은 props를 state로 넣을 수 있다.( 좋은 방법은 아님 )  
  `const [result,setResult] =useState(tryInfo.result); `  
  ` setResult('1');`

* constructor 를 사용 하는 경우 (미세한 컨트롤이 가능하다.)
